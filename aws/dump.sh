#!/bin/bash

DB_HOST=$1
DB_PASSWORD=$2
DB_NAME=$3

DATE=$(date +%d-%m-%Y_%H:%M:%S)

AWS_ID=$4
AWD_PASS=$5

BUCKET_NAME=$6

mysqldump -u root -h $DB_HOST -p$DB_PASSWORD $DB_NAME > /tmp/db:$DB_NAME[$DATE].sql && \
export AWS_ACCESS_KEY_ID=$AWS_ID && \
export AWS_SECRET_ACCESS_KEY=$AWD_PASS && \
aws s3 cp /tmp/db:$DB_NAME[$DATE].sql s3://$BUCKET_NAME